# Project description

This project is to compute the __regression coefficients of floor and stack variation__ of individual Condo or HDB projects.
It queries `data_science.valuation_base` and for a dataset for an individual Condo/HDB project, trains a Random Forest model on the data, and then plots partial dependence plots (PDP) and stores the coefficients returned by the PDP function as a csv file. 

One can examine on a per-project basis, how varying the floor and stack will impact the valuation price by means of the PDP values.


## What are partial dependence plots?

Christopher Molnar describes PDPs in his book: [Interpretable Machine Learning](https://christophm.github.io/interpretable-ml-book/pdp.html).

The partial dependence plot (short PDP or PD plot) shows the marginal effect one or two features have on the predicted outcome of a machine learning model. So we find the partial dependence of varying feature X_i on Y. In a linear model, PDP is always a straight line. In non-linear models (like RF), PDP is non-linear, and allows for more interpretability.

PDP is a technique in the family of feature importances amongst PDP, ICE (indiv conditional expectation) and ALE (Accumulated Local Effects) plots.
PDP is simply an average of all ICE realizations. PDP suffers when features are correlated, and ALE less so. Molnar recommends ALE as rule of thumb. However, implementation for ALE is trickier (`ALEplot`) compared to just using `sklearn`.

# Project structure

* coefficients - floor and stack coefficients in csv format
* input - data in csv format
* notebooks - Jupyter notebooks for commentary and full ML stuff, with actual PDP plots, distributions of data, etc.
* num_units - `units.csv` which shows the distribution of num of transactions per project.
* src - the `.py` scripts. Config (), SQL (), `run_query_class.py` (), `train_and_pdp.py` ()


# Setup and Usage:

__Setup__

_Installation_ 

```python
pip install -r requirements.txt
```
_Change filepath string variables_ in `src/config/__init__.py`. Change `user_filepath` to the directory you keep the project folder in and change `project_name` to the name of the project folder.

```python
#These variables are in src/config/__init__.py
user_filepath = r'C:\Users\Ryan\Desktop\python\4-REA-Intern'
project_name = 'regression3'
```

__Usage__

_Input_ 

1. Run `run_query_class.py`. This prompts you for a input: either `condo` or `hdb`. This should query `ds.vb` and return a `condo.csv` (all condo data) and `condo_stack_index.csv` (comes in handy later) OR `hdb.csv` (all HDB data).

2. Run `train_and_pdp.py` and input either `condo` or `hdb`.  This will load in the csv files and train models iteratively on each `id` in `IDList`, then output the coefficients for all `ids` into `coefficients`. You need to supply a list of IDs for the for loop, which I just grabbed from `num_units` and loaded in 4 just to test. Since majority of the projects have very small number of rows, they have to be combined first into larger datasets. For now, the test_ID_list in the code is simply the 4 largest HDB datasets. The grid-search CV procedure is probably the bottleneck, so reducing the number of combinations of parameters will speed up the training at the expense of a small decrease in performance.

These are the features for either type. 
```python
#patsy dmatrices string
condo_str = 'transaction_amount ~ transaction_year + transaction_month + address_num + address_stack + address_floor + floor_area_sqft + num_of_bedrooms'
hdb_str = 'transaction_amount ~ transaction_year + transaction_month + min_address_floor + max_address_floor + floor_area_sqft + num_of_bedrooms + hdb_room_type'
```
Output (coefficients)

|  | index | feature | value | project_name |
| ------ | ------ | ------ | ------ | ------ |
| 1 | 0 | 1 | 0 | e022cb640ae8809689b66b0eb6464305 |
| 2 | 1 | 4 | 0.507 | e022cb640ae8809689b66b0eb6464305 |
| 3 | 2 | 7 | 2.58 | e022cb640ae8809689b66b0eb6464305 |

This is the coefficient output csv. __DESCRIPTION__ - Column 0 - .csv level index. Column 1 - project level index. Column 2 - feature (either floor or stack. For HDBs with no stack, this is an `int` representing `hdb_room_type`).
Column 3 - percentage change of row `n` relative to row 1 (Do note that the value is in `%`, so `0.507` means `0.507%` and not `50.7%`.) Column 4 - project name.

Note for condo stack, the stacks are from multiple blocks. Fortunately, each block has a disjoint set of stacks, so block A will have stack 1-7, block B will have stack 8-15, block C will have stack 16-25, etc. So we can split the stacks into ordered partitions for condos. How do we know where to partition?

|  address_num  | rolling_stack_index |
| ------ | ------ |
| 351 | 4 |
| 352 | 8 |
| 353 | 12 |
| 354 | 16 |
| 355 | 20 |

The `condo_address_stack_index.csv` is above table for condos for reference. `address_num` is block, and `rolling_stack_index` is the cumulative sum. So block 351 will have stacks 1-4, 352 will have stacks 5-8, 353 will have 9-12, etc. This is to check which stack corresponds to which block.
I am trying to add an extra column to the `coefficients` output that labels the block number of each stack, which involves using the above table, but I'm still working out the details.

# Notebooks
For the full procedure please see the notebooks, which also plots the PDP. 

# Other points:

## Model choice and performance.

The choices for this regression task were OLS, RF and XGBoost. OLS is done in the Jupyter notebook with `StatsModels`. However, OLS does not allow for nonlinear PDP, so it wasn't used in `src`. Interestingly, larger datasets tend to violate OLS assumptions while in smaller datasets (>100 rows), OLS assumptions seem to be met in some strange reverse CLT style.

Between RF and XGB, XGB always performs better both on train and test (see notebooks). However XGB has an issue with PDP (see the plots in notebooks), so RF was used. For full details on model performance, see notebooks.

## OLS and sample size

As sample size decreases, OLS assumptions are met increasingly and OLS outperforms other models. See notebooks for the OLS. This might be due to the fact that the data are linear in smaller projects (???) and non-linear in larger ones. Not too sure.

## Model issues

However there are several issues regarding the data. 

1. __Project-level data size__ - The distribution of number of past transactions per project since 2015 (our dataset restrictions) is uneven, resembling a PDF of a Pareto distribution. Majority of the projects have datasets that are too small for a model to be trained on, resulting in very high bias. (Insert image of histogram here). Thus, one would probably have to use clustering to group similar inter-project transactions to form datasets that are sufficiently big (few hundred rows minimum). The distribution is stored in `units`. For HDB, the largest sample size is 86. This is still very small. Best RF gives RMSE of 40k, and mean of dataset is 90k, so error is akin to tossing a coin. Best OLS gives RMSE of 30k. 

2. __Numeric (integer encodings)__ - To encode `address_num` and `address_stack` which are categorical features I used integer encodings. There is debate on whether binarizing them (creating a sparse matrix) is a better approach, but `sklearns partial_dependence` module does not work with OHE. However, the performance of the model trained with numeric vs OHE is quite similar (see `notebooks`).

