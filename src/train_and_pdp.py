
"""
Description: If the suitable_for_OLS flag is false, then we run this, which will train RF and XGBoost models and pickle the one with the best CV results.
Either way, projects to models should be an injective relation (aka one-to-one)
Flow: TTS --> CV --> Choose best model for RF and XGBoost --> test on test set --> choose model with higher score.
"""


import time
start = time.time()

#-----------------------------------------imports--------------------------------------#
#local
import config as config

#standard library
import math
import os

#data stuff
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
from patsy import dmatrices

#model stuff
from sklearn.model_selection import KFold, GridSearchCV, train_test_split
from sklearn.inspection import partial_dependence
from sklearn.ensemble import RandomForestRegressor

#-----------------------------------------lovely functions <3 --------------------------------------#
def grid_object(x,y, estimator, params, scoring, cv):
    """returns a fitted object"""
    grid = GridSearchCV(estimator = estimator, param_grid = params, scoring = scoring, cv = cv,
                           n_jobs = -1, return_train_score = True)
    grid.fit(x,y)
    return grid

def report_GridSearchCV_results(grid):
    print(f"-Best mean_test_score square root: {math.sqrt(abs(grid.best_score_))}\n")
    return grid.best_estimator_

def calc_pdp(model,x,features, id):
    '''this function returns the pdp coefficients for a particular project in a dataframe'''
    predictions, values = partial_dependence(model,x,features = features)
    pdp_coefficients = pd.DataFrame({"feature":np.array(values).flatten(),"value":np.array(predictions).flatten()})
    pdp_coefficients.value = ((pdp_coefficients.value/pdp_coefficients.value[0])-1)*100
    pdp_coefficients["project_name"] = id
    return pdp_coefficients

def floor_pdp(model,x, id):
    if property == 'condo':        
        floor_coefficients = calc_pdp(model,x,"address_floor", id)
    if property == "hdb":
        floor_coefficients = calc_pdp(model,x,"min_address_floor", id)
    return floor_coefficients

def stack_pdp(model,x, id):    
    if property == 'condo':        
        stack_coefficients = calc_pdp(model,x,"address_stack", id)
    if property == "hdb":
        stack_coefficients = calc_pdp(model,x,"hdb_room_type", id)
    return stack_coefficients

def bedroom_pdp(model,x, id):
    bedroom_coefficients = calc_pdp(model,x,"num_of_bedrooms", id)
    return bedroom_coefficients

#----------------------------------------------end---------------------------------------------------#


#variables outside loop
property = input("Condo or HDB? If Condo, type 'condo', if HDB, type 'hdb' : ")
datanamespace = os.listdir(fr'{config.user_filepath}\{config.project_name}\input\{property}')
condo_str = 'transaction_amount ~ transaction_year + transaction_month + address_num + address_stack + address_floor + floor_area_sqft + num_of_bedrooms'
hdb_str = 'transaction_amount ~ transaction_year + transaction_month + min_address_floor + max_address_floor + floor_area_sqft + num_of_bedrooms + hdb_room_type'

#load in the big df
df = pd.read_csv(fr'{config.user_filepath}\{config.project_name}\input\{property}\{property}.csv')

#For testing purposes we will just limit it to say, 20 HDB estates and see how the loop runs. So we need to feed in a list of IDs for the loop to run.
#If we want all IDs, then just use this.

condo_ids = list(set(pd.read_csv(fr'{config.user_filepath}\{config.project_name}\num_units\condorows.csv').dw_ds_estate_id.values))
hdb_ids = list(set(pd.read_csv(fr'{config.user_filepath}\{config.project_name}\num_units\hdbrows.csv').dw_ds_estate_id.values))

IDList = ['e022cb640ae8809689b66b0eb6464305',
            'd99a97e81d6705b13392b0475f578b1d',
            '3175d260410733e0993ac28ee5791bfe',
            '323b0d2426efb58746b5074c886b839e']
#empty bois
floor_df = []
stack_df = []
bedroom_df = []

#------------------------------------------------------------------ LOOP GOES HERE-------------------------------------------------------------------------#

for id in IDList:
    
    #individual project dataframe.
    indiv_project_df = df[df.dw_ds_estate_id.values == id]  
    
    #setting up the x and y dataframes
    if property == 'condo':
        y, x = dmatrices(condo_str, data=indiv_project_df, return_type='dataframe')
    if property == 'hdb':
        y, x = dmatrices(hdb_str, data=indiv_project_df, return_type='dataframe')

    #train test split
    x_train, x_test,y_train,y_test = train_test_split(x,y,test_size =0.2)
    #folds
    kf5 = KFold(n_splits=5,shuffle=True,random_state=0)
    #loss
    mse = "neg_mean_squared_error"
    #parameter grids
    rf_params = {"max_depth": [2,7,10,50,None],
                "min_samples_leaf": [1,10,20,30],
                'max_features': [2, 3, 5, "auto"]}

    #models
    rf = RandomForestRegressor(random_state=0)
    rf = grid_object(x_train, np.array(y_train).ravel(),rf,rf_params,mse,kf5)
    best_rf = report_GridSearchCV_results(rf)

    floorcoefficients = floor_pdp(best_rf,x, id)
    stackcoefficients = stack_pdp(best_rf,x, id)
    floor_df.append(floorcoefficients)
    stack_df.append(stackcoefficients)

    if property == 'condo':
        bedroomcoefficients = bedroom_pdp(best_rf,x, id)
        bedroom_df.append(bedroomcoefficients)
        
    print(f"{id} has completed. \n")

    
#----------------------------------------------------------------------END LOOP----------------------------------------------------------------------------#

floor = pd.concat(floor_df).reset_index()
stack = pd.concat(stack_df).reset_index()

if property == 'hdb':
    IndexToRoomTypeDict = {'0':'hdb studio apartment - type b', '1':'hdb 1 room', '2':'hdb 2 room', '3':'hdb 3 room', '4':'hdb 4 room', '5':'hdb 5 room', '6':'hdb multi-generation', '7':'hdb executive'}
    stack.feature.map(IndexToRoomTypeDict)

#export the full bois
floor.to_csv(fr'{config.user_filepath}\{config.project_name}\coefficients\floor\{property}.csv')
stack.to_csv(fr'{config.user_filepath}\{config.project_name}\coefficients\stack\{property}.csv')
     
if property == 'condo':
    bedroom = pd.concat(bedroom_df).reset_index()
    bedroom.to_csv(fr'{config.user_filepath}\{config.project_name}\coefficients\bedroom\{property}.csv')


end = time.time()

print(f"ORD lo! Script has completed in {end-start} seconds or {(end-start)/60} minutes.")
