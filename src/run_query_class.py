

"""
Description: This program will query the DB and return the dataset in the input folder.
"""
import os
import pandas
from rea_python.main.database import DBCopyMode, RedshiftHook
from rea_python.constants import OutputFormat
import config as config

#initialize redshift hook
src_hook = RedshiftHook(iam_role_arn=config.IAM_ROLE_ARN,
                                     via_s3_bucket=config.REDSHIFT_HOOK_BRIDGE_BUCKET,
                                     via_s3_folder=config.REDSHIFT_HOOK_BRIDGE_FOLDER)

#set up connection
src_hook.set_conn_from_uri(config.DATA_WAREHOUSE_URI)

#load queries from sql 
src_hook.load_queries_from_folders([fr'{config.user_filepath}\{config.project_name}\src\sql'])


class query_object:
    """
    Try class
    """
    def __init__(self, property, datanamespace):
        self.property = property
        self.datanamespace = datanamespace
        self.filepath = fr'{config.user_filepath}\{config.project_name}\input\{self.property}\{self.property}.csv'

    def query_func(self):
        
        if self.property == 'condo':
            query_str = 'condo_query'
        else:
            query_str = 'hdb_query'

        project_csv = src_hook.execute_loaded_query(
        query_name=query_str,
        output_format=OutputFormat.pandas,
        )

        return project_csv    
    
    def return_project_csv(self):
        
        project_csv = self.query_func()
        print(f"Number of rows in this dataset is {project_csv.shape[0]}.")
        project_csv.to_csv(self.filepath)

        print("Query stored in input folder.")
        return 1


def get_condo_address_stack_index():
    condo_address_stack_index = src_hook.execute_loaded_query(
        query_name='condo_address_stack_query',
        output_format=OutputFormat.pandas
        )
    condo_address_stack_index.to_csv( fr'{config.user_filepath}\{config.project_name}\input\condo\condo_stack_index.csv')
    return 1

#user input
property = input("Condo or HDB? If Condo, type 'condo', if HDB, type 'hdb' : ")
datanamespace = os.listdir(fr'{config.user_filepath}\{config.project_name}\input\{property}')


#instantiate
query = query_object(property, datanamespace)

#call the method
query.return_project_csv()

#condo address stack index
if property == 'condo' and not any('index' in name for name in datanamespace):
    condo_address_stack_index = get_condo_address_stack_index()






