-- name: condo_query
select
	vb.dw_ds_estate_id,
	vb.transaction_year,
	vb.transaction_month,
	vb.address_stack,
	vb.address_floor,
	vb.floor_area_sqft,
	vb.num_of_bedrooms,
	vb.transaction_amount*hi.hi as transaction_amount 
from 
	data_science.valuation_base vb 
	inner join data_science.sale_hi hi  on ((vb.dw_ds_estate_id = hi.dw_project_id) and (vb.transaction_month_index = hi.transaction_month_index))
where  
	vb.transaction_type = 'sale' and
	vb.country = 'sg' and 
	vb.property_type_group = 'Condo' and
	vb.address_stack is not null and
	vb.address_floor is not null and
	hi.hi is not null 
order by
	vb.dw_ds_estate_id ,vb.transaction_date 
;

-- name: hdb_query
select  
	vb.dw_ds_estate_id,
	vb.transaction_year,
	vb.transaction_month,
	p.min_address_floor,
	p.max_address_floor,
	vb.floor_area_sqft,
	vb.num_of_bedrooms,
	case 
		when vb.hdb_room_type = 'hdb studio apartment - type b' then '0'
		when vb.hdb_room_type = 'hdb 1 room' then '1'
		when vb.hdb_room_type = 'hdb 2 room' then '2'
		when vb.hdb_room_type = 'hdb 3 room' then '3'
		when vb.hdb_room_type = 'hdb 4 room' then '4'
		when vb.hdb_room_type = 'hdb 5 room' then '5'
		when vb.hdb_room_type = 'hdb multi-generation' then '6'
		when vb.hdb_room_type = 'hdb executive' then '7' end
	as hdb_room_type,
	vb.transaction_amount*hi.hi as transaction_amount
from 
	data_science.valuation_base vb
	inner join datamart.view_property_expand p using(dw_property_id)
	inner join data_science.sale_hi hi on ((vb.dw_ds_estate_id = hi.dw_project_id) and (vb.transaction_month_index = hi.transaction_month_index)) 
where
	vb.country = 'sg' and 
 	vb.property_type_group = 'HDB' and
	vb.data_origin = 'sg_gov_resale' and
	hi.hi is not null
order by 
	vb.dw_ds_estate_id , vb.transaction_date
;

-- name: condo_address_stack_query
select 
	dw_ds_estate_id,
	address_num,
	sum(address_stack_index) over (partition by dw_ds_estate_id order by address_num  rows between unbounded preceding and current row) as rolling_stack_index
from(
	select
		vb.dw_ds_estate_id,
		vb.address_num,
		count(distinct vb.address_stack) as address_stack_index
	from 
		data_science.valuation_base vb 
		inner join data_science.sale_hi hi  on ((vb.dw_ds_estate_id = hi.dw_project_id) and (vb.transaction_month_index = hi.transaction_month_index))
	where  
		vb.transaction_type = 'sale' and
		vb.country = 'sg' and 
		vb.property_type_group = 'Condo' and
		vb.address_stack is not null and
		vb.address_floor is not null and
		hi.hi is not null 
	group by 
		vb.dw_ds_estate_id , vb.address_num
	order by 
		vb.dw_ds_estate_id , vb.address_num)
order by 
	dw_ds_estate_id , address_num 
		;




